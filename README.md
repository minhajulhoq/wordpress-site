**Wordpress Site**
---

Below are the steps that can be followed to launch a wordpress site on ubuntu server.
---

### 1. Machine steps

* Download the ubuntu desktop iso file from this [link](https://ubuntu.com/download/desktop/thank-you?version=20.04.3&architecture=amd64).
* Create virtual ubuntu linux machine (64bit) on Vmware workstation 16.
* After creating load the machine with downloaded ubuntu iso file from settings.
* Run the created Vm machine.
---

### 2. Terminal steps

* Open the terminal on ubuntu.
* Download package information from all configured sources by below command.
```
sudo apt update -y

```
* Download and install nginx packages

```
sudo apt install nginx -y

```
* Now start the nginx service from below command

```
sudo systemclt start nginx

```
* Check the service status if its running or not
```
sudo systemctl status nginx

```

* Get the host ip address by
```
ip a

```
* Check the address with your browser if nginx is working properly.
---

### 3. Database steps

* Install the databse with mariadb and then start and enable the service by following commands-
```
sudo apt install mariadb-server
sudo apt install mariadb-client
sudo systemctl start mariadb.service
sudo systemctl enable mariadb.service

```
* Secure the database server with a root password-
```
sudo mysql_secure_installation

```
* Login to the database console and you will be in database directory
```
sudo mysql -u root -p

```
* You can now create the database by following the below command-
```
create database wordpress;

```
* Then create user with password set
```
create user "wpuser"@"localhost" identified by "ubuntu";

```
* Give privileges to user
```
grant all privileges on wordpress.* to 'wpuser'@'localhost';

```
* Now reload the grant table after giving privilege and then exit from database
```
flush privileges;
exit

```
* Install php packages with some extensions including
```
sudo apt install php7.4 php7.4-gd php7.4-mysql php7.4-zip php7.4-fpm -y

```
---

### 4. Wordpress steps

* Download wordpress packages by this following command
```
wget install https://wordpress.org/latest.zip

```
* Unzip the downloaded zip file and it will be found in following command-
```
unzip latest.zip
cd wordpress/

```
* Copy and host the coppied files for nginx to get from the following directory
```
sudo cp -r * /var/ww/html/

```
* Give permission to root for customization
```
sudo chown -R www-data:www-data /var/www/html/

```
* Restart the nginx service
```
sudo systemctl restart nginx

```
* Go to `sites-available`-
```
cd /etc/nginx/sites-available

```
* and move the default to *wordpress*
```
sudo mv default wordpress

```
* Edit the *wordpress* with ***nano***
```
sudo nano wordpress

```
and add `index.php` file, so the changes will be like below
```
server {
    listen 80;
    listen [::]:80;
    root /var/www/wordpress;
    index  index.php index.html index.htm;
    server_name  example.com www.example.com;

    client_max_body_size 100M;
    autoindex off;
    location / {
        try_files $uri $uri/ /index.php?$args;
    }

    location ~ \.php$ {
         include snippets/fastcgi-php.conf;
         fastcgi_pass unix:/var/run/php/php8.0-fpm.sock;
         fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
         include fastcgi_params;
    }
}


```
* Restart nginx service now
```
sudo systemctl restart nginx.service

```
* Test the configuration file for check if any issue is there
```
sudo nginx -t

```
## 5. Refresh the browser and wordpress is live!
